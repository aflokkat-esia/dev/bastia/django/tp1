from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

# Create your views here.

def home(request: HttpRequest) -> HttpResponse:
    context = { 
        "name": "Quentin", 
        "age": 30,
        "activities": [
            {
                "name": "Sport",
                "display": True
            },
            {
                "name": "Musique",
                "display": True
            },
            {
                "name": "Programming",
                "display": False
            }
        ]
    }
    return render(request, 'myapp/pages/home.html', context)